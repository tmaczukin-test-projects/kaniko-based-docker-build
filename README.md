# Kaniko-based Docker build

This test project is created in purpose of testing how to
build a Docker image, using:

- GitLab CI
- GitLab Runner, with:
    - Docker executor (without `privileged` setting)
    - Kubernetes executor (without `privileged` setting)
- [Kaniko](https://github.com/GoogleContainerTools/kaniko)

## Author

Tomasz Maczukin, 2018

## License

MIT

